/* Michael Power / St. Joseph Autonomous 2019

   This file is the program for the 2019 Skills Canada Nationals Competition.

   Table of Contents:

      - Halifax   = Contains the main setup and loop methods, and set variables
      - Base      = Contains the Basic Functions that are used to complete the problem
      - Task      = Contains the function used to complete the National Task
  
 */

// Libraries
#include <PRIZM.h>

// Variables

#define LEFT 45 //44
#define RIGHT -45 //44.5

#define pi 3.1415926359 // Defining pi cause PI isn't accurate
#define SENSOR_DELAY 20 // Time in between sensor checks

// Wall Follower Variables
// !!!!!!!  UNITS IN CM  !!!!!!!
#define END_WALL_DIST 40 // Sets the distance from wall at which code ends
#define MAX_WALL_DIST 22 // Sets the range of the wall follower (outside)
#define MIN_WALL_DIST 18 // Sets the range of the wall follower (inside)

// Port numbers
#define RIGHT_MOTOR 2 // Right Motor Number
#define LEFT_MOTOR 1 // Left motor number

/*
 * !!!!!!!!!
 * !!!!!!!!!
 * !!!!!!!!!
 * !!!!!!!!!
 */

// Change port numbers and add other Sensor port #s here
#define SONIC_SENSOR 2
#define IR_SENSOR 3

/*
 * !!!!!!!!!
 * !!!!!!!!!
 * !!!!!!!!!
 * !!!!!!!!!
 */

// Speeds
#define MAX_SPEED 161 //  0 to 720 degrees per second (DPS)
#define TURN_SPEED 90 //  0 to 720 degrees per second (DPS)
#define WALL_SPEED_FORWARD 100 // Forward Speed for Wall Follower
#define WALL_SPEED_OVER 55 // Over rotation Speed for Wall
#define WALL_SPEED_UNDER 50 // Under rotation Speed
#define CLAW_SERVO_SPEED 40 // In percentage where 100% is max speed
#define LIFT_SPEED 150 // Lift Speed, 0 to 720 degrees oer second (DPS)

// Robot dimensions
#define WHEEL_WIDTH 4.0 // the wheel is 4 inches tall (Diameter)
#define WHEEL_C (pi * WHEEL_WIDTH) // the wheel circumference in inches

/*
 * !!!!!!!!!
 * !!!!!!!!!
 * !!!!!!!!!
 * !!!!!!!!!
 */

#define WHEEL_BASE_WIDTH 12.0 // the distance between the wheels in inches

/*
 * !!!!!!!!!
 * !!!!!!!!!
 * !!!!!!!!!
 * !!!!!!!!!
 */

#define WHEEL_BASE_C (pi * WHEEL_BASE_WIDTH) // the circumference of the weel base in inches
#define COG_DIAMETER 0.745 // Pitch Diameter of the Lift Pinon (Gear)
#define COG_C (pi * COG_DIAMETER) // Circumference of the Lift Pinon's (Gear) Pitch Circle

PRIZM p; // Prizm Library Instance

void setup() {
  // put your setup code here, to run once:
  p.PrizmBegin();
  p.setMotorInvert(1, 1); // Inverts the direction. Motor # (1 or 2), Invert (0 or 1)
  p.setServoPosition(1, 87);
}

void loop() {
  // put your main code here, to run repeatedly:
  Task();
  exit(0);
}

void stoppp(){
  p.setMotorPowers(125, 125);
}

void Task(){
  forwardBy(13, MAX_SPEED);
  p.setMotorPowers(125, 125);
  rotate(-45.25, TURN_SPEED);
  delay(50);
  forwardBy(23, MAX_SPEED);
  stoppp();
  rotate(44.5, TURN_SPEED);
  delay(50);
  forwardBy(23, MAX_SPEED);
  p.setServoSpeed(1, 50);
  delay(100);
  //!!!!!!!!!!!!!!!!!!!!!!
  p.setServoPosition(1, 66);
  //!!!!!!!!!!!!!!!!!!!!!!
  delay(500);
//  p.setServoSpeed(1, 50);
  delay(500);
  p.setServoPosition(1, 87);
  delay(100);
//  p.setServoSpeed(1, 100);
  backwardBy(22, MAX_SPEED);
  stoppp();
  rotate(-44.5, TURN_SPEED);
  delay(50);
  forwardBy(22, MAX_SPEED);
  stoppp();
  rotate(LEFT, TURN_SPEED);
  //!!!!!!!!!!!!!!!!!!!!!!
  // POTENTIALLY REPLACE WITH ULTRASONC
  //!!!!!!!!!!!!!!!!!!!!!!
  delay(50);
  forwardBy(44, MAX_SPEED);
  stoppp();
  //!!!!!!!!!!!!!!!!!!!!!!
  rotate(45.5, TURN_SPEED);
  delay(50);
  forwardBy(44, MAX_SPEED);
  stoppp();
  rotate(LEFT, TURN_SPEED);
  forwardBy(22, MAX_SPEED);
  stoppp();
  rotate(RIGHT, TURN_SPEED);
  delay(50);
  forwardBy(22, MAX_SPEED);
  stoppp();
  rotate(46, TURN_SPEED);
  delay(50);
  forwardBy(24, MAX_SPEED);
  delay(100);
  //!!!!!!!!!!!!!!!!!!!!!!
  p.setServoPosition(1, 108);
  //!!!!!!!!!!!!!!!!!!!!!!
  delay(500);
//  p.setServoSpeed(1, 50);
  delay(500);
  p.setServoPosition(1, 87);
  delay(100);
  backwardBy(68, MAX_SPEED);
  rotate(LEFT, TURN_SPEED);
  delay(50);
  forwardBy(67, MAX_SPEED);
  stoppp();
  rotate(RIGHT, TURN_SPEED);
  delay(50);
  backwardBy(16, MAX_SPEED);
  stoppp();
}
