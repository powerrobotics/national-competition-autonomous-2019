/* Michael Power / St. Joseph Autonomous 2019
 
  This file contains the basic actions the robot can 
  preform. These actions are to be used in other tabs 
  to make the robot do interesting things.
*/

void rotate(double deg, int spd) {
  p.resetEncoders(); // reset encoders to clear the previous rotations

  // Using the circumference of the base, calculate how far the robot needs to spin to turn deg

  double rotateDistance = (deg / 360.0) * WHEEL_BASE_C;

  // Convert the distance needed to turn to degrees the wheels must turn
  double degreesToTurn = rotateDistance / WHEEL_C * 360;

  // Add the degrees to turn to the wheel
  double rightDeg = degreesToTurn * 4.0;
  double leftDeg = -degreesToTurn * 4.0;

  // Tell the robot to make the adjustments at a spd
  //p.setMotorDegrees(spd, rightDeg, spd, leftDeg);
  p.setMotorTargets(spd, rightDeg, spd, leftDeg);

  while(p.readMotorBusy(RIGHT_MOTOR) || p.readMotorBusy(LEFT_MOTOR)) {
    delay(SENSOR_DELAY);
  }
  //delay(100);
}

// Moves the robot forward at a spd for a time in mills
void forward(int mills, int spd) {
  p.setMotorSpeeds(MAX_SPEED, MAX_SPEED);
  delay(mills);
  p.setMotorSpeeds(0, 0); // turns the moters off when done
}

// Moves forward by inches at a spd
void forwardBy(double inches, int spd) {
  double deg = inches / WHEEL_C * 180.0;
  p.resetEncoders(); // reset encoders to clear the previous rotations
  p.setMotorDegrees(spd, deg, spd, deg);

  while(p.readMotorBusy(RIGHT_MOTOR) || p.readMotorBusy(LEFT_MOTOR)) {
    delay(SENSOR_DELAY);
  }
}

// Moves backward by inches at a spd
void backwardBy(double inches, int spd) {
  double deg = -inches / WHEEL_C * 180.0;
  p.resetEncoders(); // reset encoders to clear the previous rotations
  p.setMotorDegrees(spd, deg, spd, deg);

  while(p.readMotorBusy(RIGHT_MOTOR) || p.readMotorBusy(LEFT_MOTOR)) {
    delay(SENSOR_DELAY);
  }
  //delay(100);  
}

void stopMotors() {
    p.setMotorSpeeds(0, 0);
}

// Follows a wall on ONE SIDE of the robot, until the wall ends
/*
void wallFollow() {
    // establish a base Sonic Sensor reading
  long dist = p.readSonicSensorCM(SONIC_SENSOR);
    // set distance at which the code will shut off; done in DEFINE
  while(dist < END_WALL_DIST){
    // set the max distance from wall to follow
    if(dist > MAX_WALL_DIST){
      p.setMotorSpeeds(WALL_SPEED_UNDER, WALL_SPEED_OVER);
    // set the min distance from wall to follow
    } else if(dist < MIN_WALL_DIST){
      p.setMotorSpeeds(WALL_SPEED_OVER, WALL_SPEED_UNDER);
    // if in the required range, go forward
    } else{
      p.setMotorSpeeds(WALL
      ?|}Z_SPEED_FORWARD, WALL_SPEED_FORWARD);
    }
    // reset the Sonic Sensor reading to what's current
    dist = p.readSonicSensorCM(SONIC_SENSOR);
  }
  stopMotors();
}
*/
// Moves forward until a line on the floor is hit by the IR Sensor.
void forwardUntilLine(int spd) {
  p.setMotorSpeeds(spd, spd);
  while(p.readLineSensor(IR_SENSOR) == 0) {}
  p.setMotorSpeeds(0, 0);
}

// Moves backward until a line on the floor is hit by the IR Sensor.
void backwardUntilLine(int spd) {
  p.setMotorSpeeds(-spd, -spd);
  while(p.readLineSensor(IR_SENSOR) == 0) {}
  p.setMotorSpeeds(0, 0);
}

// Moves forward until the Super Sonic Sensor detects a distance in CM
void forwardUntilDist(double dist_cm, int spd) {
  int current_spd = spd;
//  for (int i = 0; i < 10; i++) p.readSonicSensorCM(SONIC_SENSOR);
  long dist = 100000;
  p.setMotorSpeeds(spd, spd);
  while(dist > dist_cm) {
    dist = p.readSonicSensorCM(SONIC_SENSOR);
    Serial.println(dist);
    delay(2);
  }
  p.setMotorSpeeds(0, 0);
}
